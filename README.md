# GBios - Galaxi Basic IO System
GBios is a ROM Resource Pack for ComputerCraft.
Designed by KernlORG Galaxi to be as minimal as possible.
### How does it work


```
Computer turns on
 |
bios.lua
 |
/rom/programs/advanced/multishell or /rom/programs/shell
This step doesn't execute the actual shell/multishell program, it's just a symlink to the next step.
 |
/rom/autogen
 |
/vmgalaxi
```

## Building GBios from source
If you modified the code, you can build GBios to get a compiled resource pack with the files you'll need.
Just use `./compile.sh` and that will generate a file called `gbios-build.zip`
And, if you want files to be automatically put to rootpath if /vmgalaxi doesn't exist, you can use the `--autoroot` flag with `compile.sh`, that will create /rom/autogen, to be run just after bios.lua, and that will copy the files for you. You can place the files you want in /rom/root.

## What is recovery mode
Since 0.4, when shell or multishell cannot find /vmgalaxi, it tries to check for /rom/vmgalaxi and /rom/root/vmgalaxi, if none is found, it will fallback to /rom/stock/vmgalaxi, that will execute a recovery shell. In this recovery shell you have to use absolute paths and you should try to fix the problem by making a custom /vmgalaxi.

KernlORG Galaxi(c) 2016
