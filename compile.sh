#!/bin/bash
# GBios compile script

mkdir -p ../gbiosz/assets/computercraft/lua/rom/
cp -r ./* ../gbiosz/assets/computercraft/lua/rom/
if [[ "$1" == "--autoroot" ]]; then
  echo "Using autoroot mode"
  cat <<EOT >> ../gbiosz/assets/computercraft/lua/rom/autogen
  if not fs.exists("/vmgalaxi") then os.run("cp /rom/root* /") end
EOT
  cp -r ./root/* ../gbiosz/assets/computercraft/lua/rom/root/
fi
cd ../gbiosz
zip -r ../gbios/gbios-build.zip ./
rm -rf ../gbiosz
echo "Compiled successfully into gbios-build.zip"
